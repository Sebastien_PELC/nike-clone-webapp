import React from 'react';
import Card from "../Card/Card";

function HomePageCarousel(props) {
    return (
        <section
            className='w-full h-[450px] relative bg-white flex space-y-2'
        >
               <Card
               src={'/Images/dunk_retro.png'}
               alt={'Card'}
               title={`Nike Dunk Low Retro`}
               subtitle={`Chaussure pour Homme`}
               price={`119,99`}
               ></Card>
               <Card
                   src={'/Images/dunk_low.png'}
                   alt={'Card'}
                   title={`Nike Dunk Low`}
                   subtitle={`Chaussure pour Ado`}
                   price={`94,99`}
               ></Card>
               <Card
                   src={'Images/airmax.png'}
                   alt={'Card'}
                   title={`Nike Dunk Low Retro`}
                   subtitle={`Chaussure pour Femme`}
                   price={`119,99`}
               ></Card>
        </section>
    );
}

export default HomePageCarousel;