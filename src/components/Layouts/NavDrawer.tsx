import React from 'react';
import {Bars3Icon} from "@heroicons/react/16/solid";

function NavDrawer(props) {
    return (
        <div>
            <button>
                <Bars3Icon
                className={`icon-button`}>

                </Bars3Icon>
            </button>
        </div>
    );
}

export default NavDrawer;