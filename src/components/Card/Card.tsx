import React from 'react';
import Image from 'next/image';
import Link from "next/link";

function Card({src, alt, title, subtitle, price}: {
    src: string,
    alt: string,
    title: string,
    subtitle: string,
    price: string
}) {
    return (
        <div
            className={`w-1/3 flex flex-col bg-white p-8`}
        >
            <div
                className={`relative w-full h-full`}
            >
                <Link
                href={`/products`}
                >
                    <Image
                        src={src}
                        alt={alt}
                        fill
                        className='object-cover'
                    >
                    </Image>
                </Link>
            </div>
            <div
                className={`flex flex-col`}
            >
                <span
                    className={`text-black font-medium text-base`}
                >{title}</span>
                <span
                    className={`text-[#757575] font-regular text-base`}
                >{subtitle}</span>
                <span
                    className={`text-black font-medium text-base`}
                >{`${price} €`}</span>
            </div>
        </div>
    );
}

export default Card;