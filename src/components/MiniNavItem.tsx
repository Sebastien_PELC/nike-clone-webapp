import React from 'react';
import Link from "next/link";
import NavItem from "./NavItem";
type Props = {
    link: string
    text: string
}
function MiniNavItem(props: Props) {
    return (
        <li
            className='pl-3 text-xs font-medium text-base'
        >
            <Link
                href={props.link}
                className='hover:text-[#757575]'
            >
                       <span className={'font-medium '}>
                       {props.text}
                   </span>
            </Link>
        </li>
    );
}

export default NavItem