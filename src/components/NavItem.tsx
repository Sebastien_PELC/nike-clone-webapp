import React from 'react';
import Link from "next/link";
type Props = {
    link: string
    text: string
}
function NavItem(props: Props) {
    return (
        <li
            className='pl-3 font-medium text-base'
        >
            <Link
                href={props.link}
                className='hover:text-[#111111] hover:underline'
            >
                       <span className={'font-medium '}>
                       {props.text}
                   </span>
            </Link>

        </li>
    );
}

export default NavItem;