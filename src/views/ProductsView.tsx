import React from 'react';
import Layout from "../components/Layouts/Layout";
import Image from "next/image";


function ProductsView() {
    return (
            <Layout>
                <div
                    className={`w-full h-[100vh] flex`}
                >
                    <div
                        className={`relative w-1/2 h-full`}
                    >
                        <Image
                            src={`/images/dunk_retro.png`}
                            alt={`Image du produit`}
                            fill
                            className='object-cover'
                        >
                        </Image>
                    </div>
                    <div
                        className={`w-1/2 h-full`}
                    >
                        <div
                            className={`flex flex-col pl-32 py-2`}
                        >
                                <span
                                    className={`text-2xl font-medium`}
                                >Nike Dunk Low Retro</span>
                            <span
                                className={`pb-6 text-base font-medium`}
                            >Chaussure pour Homme</span>
                            <span
                                className={`pb-6 text-base font-medium`}
                            >119,99 €</span>
                        </div>
                        <div
                            className={`flex pl-32`}
                        >
                            <div
                                className={`relative w-[100px] h-[50px]`}
                            >
                                <Image
                                    src={`/images/dunk_retro.png`}
                                    alt={`Image du produit`}
                                    fill
                                    className='object-cover'
                                >
                                </Image>
                            </div>
                            <div
                                className={`relative w-[100px] h-[50px]`}
                            >
                                <Image
                                    src={`/images/dunk_retro.png`}
                                    alt={`Image du produit`}
                                    fill
                                    className='object-cover'
                                >
                                </Image>
                            </div>
                        </div>
                        <div
                            className={`pl-32 pr-16 py-4 flex justify-between`}
                        >
                            <span>
                                Selectionner la taille
                            </span>
                            <span>
                                Guide des tailles
                            </span>
                        </div>
                        <div
                            className={`pl-32 pr-16 grid grid-cols-3 gap-0.5 w-[500px]`}
                        >
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                            <div
                                className={`w-[100px] h-[50px] border-2 rounded-2xl flex items-center justify-center`}
                            >
                                <span>TEXT</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
    );
}

export default ProductsView;