import React from 'react';

import Header from "../components/Layouts/Header";
import HomePageBanner from "../components/HomePage/HomePageBanner";
import Footer from "../components/Layouts/Footer";
import AboutNav from "../components/Layouts/AboutNav";
import HomePageCarousel from "../components/HomePage/HomePageCarousel";
import Layout from "../components/Layouts/Layout";

function HomePageView() {
    return (
        <Layout>
            <HomePageBanner></HomePageBanner>
            <HomePageCarousel></HomePageCarousel>
        </Layout>
    );
}

export default HomePageView;