import React from 'react';
import Layout from "../components/Layouts/Layout";
import Image from "next/image";
import Link from "next/link";

function NotFoundView() {
    return (
        <Layout>
            <div
                className={`w-full h-full flex flex-col items-center justify-center py-10`}
            >
                <div
                    className={`w-[500px] h-[500px] relative`}
                >
                    <Image
                        src={`/images/404.jpg`}
                        alt={`Erreur 404`}
                        fill
                        className='object-cover'
                    >
                    </Image>

                </div>
                <button
                    className={`bg-black text-white px-6 py-2 rounded-full mt-2`}
                >
                    <Link href={`/`}>
                        Accueil
                    </Link>
                </button>
            </div>
        </Layout>
    );
}

export default NotFoundView;