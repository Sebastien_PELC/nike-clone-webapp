import React from 'react';
import Image from "next/image";
import Link from "next/link";
import NotFoundView from "../src/views/NotFoundView";

function notFound()
{
    return (
        <NotFoundView></NotFoundView>
    );
}

export default notFound;